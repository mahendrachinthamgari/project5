function counterFactory(counter) {
    function incrementFunction(counter) {
        return ++counter;
    }
    function decrementFunction(counter) {
        return --counter;
    }
    return { increment: incrementFunction(counter), decrement: decrementFunction(counter) }
}


module.exports = counterFactory;