const cacheFunction = require("../cacheFunction.cjs");


function callback(argument) {
    return argument + 1;
}

const invoke = cacheFunction(callback);
console.log(invoke(4));