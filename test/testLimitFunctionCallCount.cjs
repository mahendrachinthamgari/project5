const limitFunctionCallCount = require("../limitFunctionCallCount.cjs");


function callback(number) {
    console.log(number);
}

limitFunctionCallCount(callback, 5);