function limitFunctionCallCount(callback, number) {
    function limit(){
        return callback(number);
    }
    while (number > 0) {
        limit();
        number = number - 1;
    }
}


module.exports = limitFunctionCallCount;
