function cacheFunction(callback) {
    const cacheObject = {};
    function invoke(argument) {
        if (cacheObject.hasOwnProperty(argument)) {
            return cacheObject[argument];
        } else {
            const value = callback(argument);
            cacheObject[argument] = value;
            return value;
        }
    }
    return invoke;
}


module.exports = cacheFunction;